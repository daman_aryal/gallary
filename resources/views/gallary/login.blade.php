@extends('gallary.master')
@section('content')
    <div class="content">
        <div class="col-md-12">
            <h3>
                Please Login
            </h3>
        </div>
    </div>

    <div class="content">
        <div class="row">
            <div class="col-md-4">
                <form action="{{url('info/login')}}" class="form" method="post">
                {{csrf_field()}}
                    <div class="form-group">
                        <input type="text" name="name" placeholder="   Enter Your Name"
                        class="form-control" style="background-color:#f8f8ff"/>
                    </div>
                    <div class="form-group">
                         <input type="password" name="password" placeholder="   Enter Your Password" class="form-control" style="background-color:#f8f8ff"/>
                     </div>
                     <input type="submit" value="Login" class="btn btn-primary"/>
                </form>
            </div>
        </div>
    </div>
@endsection