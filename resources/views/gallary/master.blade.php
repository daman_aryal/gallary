<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>My Gallary</title>
    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{url('css/material.css')}}"/>
    <link rel="stylesheet" href="{{url('css/ripples.css')}}"/>
    <link rel="stylesheet" href="{{url('css/roboto.css')}}"/>
    <link rel="stylesheet" href="{{url('css/normalize.css')}}"/>
   {{-- <script type="text/javascript">
        var baseUrl="{{url('/')}}";
    </script>--}}
</head>
<body>
    <div class="container">
        @yield('content')
    </div>
    <script type="text/javascript" src="{{url('js/jquery-2.1.4.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery-2.1.4.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/app.js')}}"></script>



</body>
</html>