@extends('gallary.master')
@section('content')
    <h1>{{$gallary->name}} <span style="font-size: 20px">'s Gallary</span></h1>
    <a class="btn btn-primary" href="{{url('info/view')}}">Back</a>
@endsection