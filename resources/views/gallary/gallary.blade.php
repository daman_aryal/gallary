    @extends('gallary.master')
    @section('content')

        <div class="row">
            <div class="col-md-12">
                <h1>My Gallaries</h1>
            </div>
        </div>

        <div class="row">
        <div class="col-md-4">
             <form class="form" action="{{url('info/save')}}" method="post">
                                   {{csrf_field()}}
                  <input type="text"
                                   class="form-control"
                                   name="gallary_name"
                                   id="gallary_name"
                                    placeholder="Name of Gallary"
                                    value="{{old('gallary_name')}}"
                  />

                  <input type="submit" class="btn btn-primary" value="Save"/>

              </form>
        </div>
         <div class="col-md-8">

         </div>

          </div>
    @foreach($errors->all() as $error)
                      <p style="color: red"> * {{$error}}</p>
                 @endforeach


                    <div class="row">
                       <div class="col-md-12">
                             <legend>Listing will come here</legend>
                                      <table class="table table-striped table-bordered table-responsive">
                                          <thead>
                                               <tr class="danger">
                                                   <th>Gallary Name</th>
                                                   <th></th>
                                               </tr>
                                          </thead>
                                          <tbody>
                                           @foreach($gallaries as $gallary)
                                                       <tr>
                                                       <td>{{$gallary->name}}</td>
                                                       <td><a href="{{url('info/viewpics/'.$gallary->id)}}">View</a></td>
                                                       </tr>
                                            @endforeach
                                          </tbody>
                                      </table>
                       </div>
                     </div>
      @endsection

