<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->delete();
        $user=array(
            'name'=>'Daman',
            'email'=>'daman19dig@gmail.com',
            'password'=>Hash::make('hpkolaptop')
        );


        DB::table('users')->insert($user);
    }
}
