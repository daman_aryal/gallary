<?php
/**
 * Created by PhpStorm.
 * User: manmi4
 * Date: 11/15/2015
 * Time: 7:41 PM
 */

namespace App\Http\Controllers;


use App\Gallary;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class GallaryController extends Controller {
    public function getLogin(){
        return view('gallary.login');

    }

    public function postLogin(){
        $input =Input::all();

        $rules=array(
            'name'=>'required',
            'password'=>'required'
        );

        $v=Validator::make($input, $rules);

        if($v->fails()){
            return 'unvalid';
        }else{
            $credintal = array(
                'email'=>$input['name'],
                'password'=>$input['password']
            );
            $Auth=Auth::attempt($credintal);
            dd($Auth);
        }
    }

    public function getView(){
        $gallaries=Gallary::all();
        return view('gallary.gallary')->with('gallaries', $gallaries);
    }

    public function postSave(Request $request){
        /*$gallary = new Gallary();
        $input=Input::all();*/
        $rules=array(
            'gallary_name'=>'required|min:3'
        );

        $v=Validator::make($input, $rules);

        if($v->fails()){
            return Redirect::to('info/view')->withErrors($v)->withInput();
        }else{
            $gallary->name=$request->gallary_name;
            $gallary->created_by=1;
            $gallary->published=1;
            $gallary->save();

            return redirect()->back();
        }


    }

    public function getViewpics($id)
    {
        $gallary = Gallary::find($id);
        return view('gallary.viewpics')->with('gallary', $gallary);
    }

    public function postImageUpload()
    {

    }
} 